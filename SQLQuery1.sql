CREATE DATABASE EMP;
USE EMP;

CREATE TABLE Employee(
	Empid int not null primary key identity(1,1),
	Empname nvarchar(150),
	Email nvarchar(150),
	Age int,
	Salary int
);



insert into Employee(Empname,Email,Age,Salary) 
values 
( 'Angel', 'javentura96@gmail.com', 24, 20000);


insert into Employee(Empname,Email,Age,Salary) 
values 
( 'Maricruz', 'mary@gmail.com', 23, 14000);


select * from Employee;